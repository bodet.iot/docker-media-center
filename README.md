# Docker media center

**Media center à base de container Docker**

Ce projet est destiné à tous ceux qui souhaitent installer un serveur multimédia DIY (téléchargement, recherche, gestion bibliothèque, lecture, ...). Le serveur tourne sur un linux (dans mon cas ubuntu) et les applications seront des containers docker afin de faciliter l'installation et la maintenace.

Je pensais créer un dossier par besoin avec les différents containers à installer. Par exemple "Automatiser le téléchargement des fichiers" ou "Streamer les fichiers sur n'importe quel lecteur". Il y aura plusieurs possibilités en fonction des préférences de chacun (par ex Plex vs Kodi) à chaque fois je proposerai un fichier docker-compose pour installer les différents containers.

Au fur et à mesure des besoins je mettrais à jour le docker-compose général pour ceux qui souhaitent tout installer "d'un coup". Je fournirai aussi les commandes bash à passer sur le système hote.

N'hésitez pas à proposer vos améliorations ou questions.


PS : Ce projet ne concerne que la gestion des fichiers multimédia pensez à ajouter quelques containers "outils" à votre projet. Je créerais un autre projet pour ca



Tour d'horizon des containers (non définitif)

Téléchargement et recherche :
 - qBittorent (client de téléchargement torrent)
 - jdownloader (client téléchargement http)
 - jackett (proxy torrent)
 - radarr (automatiseur de téléchargement pour les films)
 - sonarr (automatiseur de téléchargement pour les séries)
 - lidarr (automatiseur de téléchargement pour les musiques)

Lecture de fichiers :
 - Plex media server (lecteur vidéo centralisé)
 - Logitech media server (lecteur audio multiroom)




