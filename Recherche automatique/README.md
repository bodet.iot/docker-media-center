Pour la recherche automatique de vidéos il existe plusieurs logiciels qui peuvent automatiser le téléchargement.
Après installation vous renseignez vos préférences vidéos et audio (langue, qualité, sous titres ...) votre client torrent et votre espace de stockage.
Il ne vous reste plus qu'à demander un fichier (audio ou vidéo) recherché, dès qu'il est disponible il l'envoie en téléchargement à votre client torrent et dès que le téléchargement
est terminé il le renomme et le classe dans le dossier paramétré.



Pour les séries il y a :
- Sonarr (plus d'infos https://github.com/Sonarr/Sonarr)
- Sick Rage


Pour les films il y a : 
 - Radarr (plus d'infos https://github.com/Radarr/Radarr)
 - Couch Potato
 
 
Pour la musique il y a :
 - Lidarr (plus d'infos https://github.com/lidarr/Lidarr)